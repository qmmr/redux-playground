import expect from 'expect'
import deepFreeze from 'deep-freeze'

// Using Object.assign
// const toggleTodo = todo => Object.assign({}, todo, { completed: !todo.completed })
// Using object spread operator proposal ES7
const toggleTodo = todo => ({ ...todo, completed: !todo.completed })

describe('Given toggleTodo function', () => {
	it('should remove the item from the array at the given index', () => {
		const TODO_BEFORE = { id: 1, text: 'Learn Redux', completed: false }
		const TODO_AFTER = { id: 1, text: 'Learn Redux', completed: true }

		expect(toggleTodo(TODO_BEFORE)).toEqual(TODO_AFTER)
	})

	it('should return a clone of the array', () => {
		const TODO_BEFORE = { id: 1, text: 'Learn Redux', completed: false }
		const TODO_AFTER = { id: 1, text: 'Learn Redux', completed: true }

		deepFreeze(TODO_BEFORE)

		expect(toggleTodo(TODO_BEFORE)).toEqual(TODO_AFTER)
	})
})
