import expect from 'expect'
import deepFreeze from 'deep-freeze'

const updateTodoItem = (state, action) => {
	switch (action.type) {
		case 'ADD_TODO':
			return [
				...state,
				{ id: action.id, text: action.text, completed: false }
			]
		case 'TOGGLE_TODO':
			if (state.id === action.id) {
				return { ...state, completed: !state.completed }
			}

			return state
		default:
			return state
	}
}

const todos = (state = [], action) => {
	switch (action.type) {
		case 'ADD_TODO':
			return updateTodoItem(state, action)
		case 'TOGGLE_TODO':
			return state.map(t => updateTodoItem(t, action))
		default:
			return state
	}
}

describe('Given a todos reducer function', () => {
	describe('when an action with type `ADD_TODO` is dispatched', () => {
		const stateBefore = []
		const action = { type: 'ADD_TODO', id: 0, text: 'Learn Redux' }
		const stateAfter = [
			{ id: 0, text: 'Learn Redux', completed: false }
		]
		deepFreeze(action)
		deepFreeze(stateBefore)

		it('should return the state with the new todo item', () => {
			expect(todos(stateBefore, action)).toEqual(stateAfter)
		})
	})

	describe('when an action with type `TOGGLE_TODO` is dispatched', () => {
		const stateBefore = [
			{ id: 0, text: 'Learn Redux', completed: false },
			{ id: 1, text: 'Wash the dishes', completed: false }
		]
		const action = { type: 'TOGGLE_TODO', id: 1 }
		const stateAfter = [
			{ id: 0, text: 'Learn Redux', completed: false },
			{ id: 1, text: 'Wash the dishes', completed: true }
		]
		deepFreeze(stateBefore)
		deepFreeze(action)

		it('should return the state with the updated version of the todo item', () => {
			expect(todos(stateBefore, action)).toEqual(stateAfter)
		})
	})
})
