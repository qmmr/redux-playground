import expect from 'expect'
import deepFreeze from 'deep-freeze'

// ES 5
// const addCounter = (list, n) => {
// 	return list.concat([ n ])
// }
const addCounter = (list, n) => [ ...list, n ]
// ES 5
// const removeCounter = (list, idx) => {
// 	return list.slice(0, idx).concat(list.slice(idx + 1))
// }
const removeCounter = (list, idx) => [ ...list.slice(0, idx), ...list.slice(idx + 1) ]
// ES 5
// const incrementCounter = (list, idx) => {
// 	return list
// 		.slice(0, idx)
// 		.concat([ list[idx] + 1 ])
// 		.concat(list.slice(idx + 1))
// }
const incrementCounter = (list, idx) => {
	return [
		...list.slice(0, idx),
		list[idx] + 1,
		...list.slice(idx + 1)
	]
}

describe('Given addCounter function', () => {
	let LIST_BEFORE, LIST_AFTER

	beforeEach(function() {
		LIST_BEFORE = []
		LIST_AFTER = [ 5 ]
	})

	it('should included the new item in the array', () => {
		expect(addCounter(LIST_BEFORE, 5)).toEqual(LIST_AFTER)
	})

	it('should return a clone of the array', () => {
		deepFreeze(LIST_BEFORE)

		expect(addCounter(LIST_BEFORE, 5)).toEqual(LIST_AFTER)
	})
})

describe('Given removeCounter function', () => {
	let LIST_BEFORE, LIST_AFTER

	beforeEach(function() {
		LIST_BEFORE = [ 5, 10, 15 ]
		LIST_AFTER = [ 5, 15 ]
	})

	it('should remove the item from the array at the given index', () => {
		expect(removeCounter(LIST_BEFORE, 1)).toEqual(LIST_AFTER)
	})

	it('should return a clone of the array', () => {
		deepFreeze(LIST_BEFORE)

		expect(removeCounter(LIST_BEFORE, 1)).toEqual(LIST_AFTER)
	})
})

describe('Given incrementCounter function', () => {
	it('should remove the item from the array at the given index', () => {
		const LIST_BEFORE = [ 5, 10, 15, 20, 25, 30 ]
		const LIST_AFTER = [ 5, 10, 16, 20, 25, 30 ]

		expect(incrementCounter(LIST_BEFORE, 2)).toEqual(LIST_AFTER)
	})

	it('should return a clone of the array', () => {
		const LIST_BEFORE = [ 5, 10, 15, 20, 25, 30 ]
		const LIST_AFTER = [ 5, 10, 16, 20, 25, 30 ]

		deepFreeze(LIST_BEFORE)

		expect(incrementCounter(LIST_BEFORE, 2)).toEqual(LIST_AFTER)
	})
})
