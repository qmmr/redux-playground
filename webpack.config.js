var path = require('path')

module.exports = {
	entry: './src/main.js',
	output: {
		path: './public/js',
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{ test: /\.jsx?$/, include: path.join(__dirname, 'src'), loader: 'babel' }
		]
	}
}
