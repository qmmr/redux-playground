import React from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'

const counter = (state = 0, action) => {
	switch (action.type) {
		case 'INCREMENT':
			return state + 1
		case 'DECREMENT':
			return state - 1
		default:
			return state
	}
}

// more or less how the createStore function looks
// const createStore = (reducer) => {
// 	let state
// 	let listeners = []
//
// 	const getState = () => state
//
// 	const dispatch = action => {
// 		state = reducer(state, action)
// 		listeners.forEach(listener => listener())
// 	}
//
// 	const subscribe = listener => {
// 		listeners.push(listener)
// 		return () => {
// 			listeners = listeners.filter(l => l !== listener)
// 		}
// 	}
//
// 	dispatch({})
//
// 	return { getState, dispatch, subscribe }
// }

const store = createStore(counter)

const Counter = ({ value, onIncrement, onDecrement }) => (
		<div>
			<h1>{ value }</h1>
			<button onClick={ onIncrement }>+</button>
			<button onClick={ onDecrement }>-</button>
		</div>
)

const increment = () => store.dispatch({ type: 'INCREMENT' })
const decrement = () => store.dispatch({ type: 'DECREMENT' })
const render = () => {
	ReactDOM.render(<Counter value={ store.getState() } onIncrement={ increment } onDecrement={ decrement } />, document.getElementById('app'))
}

store.subscribe(render)
render()
